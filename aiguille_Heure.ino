void aiguilleHeure () {
  int statePinHeure = digitalRead (pinHeure);
  bool sens = digitalRead (pinSens);

  if ( sens ) {
    if (statePinHeure != lastStatePinHeure) {
      stepper.step(-1);
      lastStatePinHeure = statePinHeure;
    }

  }
  else {
    if (statePinHeure != lastStatePinHeure) {
      stepper.step(1);
      lastStatePinHeure = statePinHeure;
    }
  }
}

