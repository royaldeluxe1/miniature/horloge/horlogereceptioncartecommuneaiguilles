#include <SoftwareSerial.h>
#include <Stepper.h>

SoftwareSerial serialReception (2, 3);

const int stepsPerRevolution = 300;  // Nombre de pas pour qu'une aiguille face un tour
Stepper stepper(stepsPerRevolution, 4, 5, 6, 7);

int minute = 0;
int pas = 1;    // nombre de pas

int pinL_DipSwitch = A4;
int pinH_DipSwitch = A5;

int pinHeure = 8;
int pinSens = 9;
int pinCapteur = 10;

int statePinHeure = LOW;
int lastStatePinHeure = HIGH;

int s1, s2, s3; //seuil du des valeurs de map
int t1, t2, t3; //valeur de la temporisation
int typeCarte;

unsigned long previousMillis = 0;        // sauvegarde last time
unsigned long currentMillis;             //temps actuel

String command;
String part1;
String part2;

int indexSecondeFolle;
int nbPasA [] = { 114, -34, 170, -28, 200, -150, 100, -80, 300, -130};

int stepspeed = 200;

void setup() {

  Serial.begin(9600);
  serialReception.begin(9600);

  stepper.setSpeed(stepspeed);

  pinMode(pinL_DipSwitch, INPUT_PULLUP);
  pinMode(pinH_DipSwitch, INPUT_PULLUP);

  pinMode(pinCapteur, INPUT);


  //********************************** configuration carte

  bool lSwitch = digitalRead (pinL_DipSwitch);
  bool hSwitch = digitalRead (pinH_DipSwitch);

  if (lSwitch & !hSwitch) {                    //  Carte Aiguille Heure
    Serial.println ("Aiguille Heure");
    pinMode(pinHeure, INPUT_PULLUP);
    pinMode(pinSens, INPUT_PULLUP);
    typeCarte = 1;
    horlogeReset (1);
  }

  else if ( ! lSwitch & hSwitch ) {
    Serial.println ("Aiguille Minutes");       //  Carte Aiguille Minutes
    pinMode(pinHeure, OUTPUT);
    pinMode(pinSens, OUTPUT);
    digitalWrite(pinHeure, HIGH);
    digitalWrite(pinSens, HIGH);
    s1 = 100; s2 = 150; s3 = 600;
    t1 = 36000, t2 = 11993, t3 = 120;
    typeCarte = 2;
    horlogeReset (1);
  }

  else if ( !lSwitch & !hSwitch ) {
    Serial.println ("Aiguille Secondes");       //  Carte Aiguille Secondes
    s1 = 100; s2 = 150; s3 = 950;
    t1 = 4000; t2 = 190; t3 = 2;
    typeCarte = 3;
    horlogeReset (1);
  }

  else if (lSwitch & hSwitch) {           //  Carte Carte emetteur
    Serial.println ("Carte emetteur");
    typeCarte = 4;
  }



  Serial.println ("fin seput");

}

void loop() {


  //********************************** Reception

  //Serial.println(part2);
  if (serialReception.available()) {

    char c = serialReception.read(); //get the character

    if (c == '\n') {
      parseCommand(command);
      //Serial.println ( command);
      command = "";
    }
    else {
      command += c;
    }
  }

  switch (typeCarte) {
       //     Serial.println(typeCarte);
    case 1:    //  //  Carte Aiguille Heure
      aiguilleHeure ();
      //Serial.println ("void heure");
      break;

    case 2:    //  Carte Aiguille Minutes
      aiguille_Seconde_Minutes ();
      //Serial.println ("aiguille minutes");
      break;

    case 3:    //  Carte Aiguille Secondes
      aiguille_Seconde_Minutes ();
      //Serial.println ("aiguille seconde");
      break;

    case 4:     //  Carte Carte emetteur
      //Serial.println ("Carte emetteur");
      break;
  }


  //**************************************  Reset

  if (part1.equalsIgnoreCase("r")) {
    int val = part2.toInt();
    horlogeReset (val);
  }

  //  ************************************   Bad commande

  else {
    //Serial.println("COMMAND NOT RECOGNIZED");
  }
}


