void aiguille_Seconde_Minutes () {  
  //********************************** Anti Horaire


  if (part1.equalsIgnoreCase("a")) {

    int val = part2.toInt();

    if (val < s1) {
      int dureTemps = (map ( val, 0, s1, t1, t2));
      if (temps (dureTemps)) {
        anti_Horaire ();
      }
    }

    else if (s1 <= val && val <= s2)  { //temps seconde
      int dureTemps = (t2);
      if (temps (dureTemps)) {
        anti_Horaire ();
      }
    }

    else if ( s2 < val && val <= s3 ) {
      int dureTemps = (map ( val, s2, s3, t2, t3));
      if (temps (dureTemps)) {
        anti_Horaire ();
      }
    }

    else if ( s3 < val ) {
      int dureTemps = (map ( val, s3, 1022, t3, 2));
      switch (typeCarte) {
        case 2:
          if (temps (dureTemps)) {
            anti_Horaire ();
          }
          break;
        case 3:
          anti_Horaire ();
          break;
      }

    }
  }




  //*********************************** Stop

  else if (part1.equalsIgnoreCase("s")) {
    int val = part2.toInt();
    //Serial.println ("Stop");
  }

  //*********************************** Sens Horaire

  else if (part1.equalsIgnoreCase("h")) {
    int val = part2.toInt();
    //Serial.println(val);

    if (val < s1) {
      int dureTemps = (map ( val, 0, s1, t1, t2));
      if (temps (dureTemps)) {
        sens_Horaire ();
      }
    }

    else if (s1 <= val && val <= s2)  {
      int dureTemps = (t2);
      if (temps (dureTemps)) {
        sens_Horaire ();
      }
    }

    else if ( s2 < val && val <= s3 ) {
      int dureTemps = (map ( val, s2, s3, t2, t3));
      if (temps (dureTemps)) {
        sens_Horaire ();
      }
    }

    else if ( s3 < val ) {
      int dureTemps = (map ( val, s3, 1023, t3, 2));

      switch (typeCarte) {
        case 2:
          if (temps (dureTemps)) {
            sens_Horaire ();
          }

          break;
        case 3:
          sens_Horaire ();
          break;
      }

    }

  }

}

